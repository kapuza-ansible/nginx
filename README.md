# Nginx install role
## Use
```bash
cd example

# Edit inventory
vim inventory/hosts

# Edit configs
vim roles/nginx/defaults/main.yml

# Install
ansible-playbook 10-nginx_install.yml

# Configure and restart
ansible-playbook 20-nginx_configure.yml
```
